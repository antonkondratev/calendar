<?php

namespace App\Http\Controllers;

use App\User;
use Google_Service_Calendar;
use http\Exception;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_People;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class AuthController extends Controller
{
    public function login(Request $request){
        if($request->isMethod('post')){
            return Socialite::driver('google')->redirect();
        }
        return view('auth.login');
    }

    public function googleCallback(Google_Client $googl,Request $request){
        $client=new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $client->addScope(Google_Service_Calendar::CALENDAR_READONLY);
        $client->addScope("email");
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $client->setApprovalPrompt('force');
        $client->setAccessType('offline');
        $client->setPrompt('select_account');
//        $client = $googl->client();
        if ($request->has('code')) {

            $client->authenticate($request->get('code'));

            $plus = new \Google_Service_Plus($client);
            $calendar = new \Google_Service_Calendar($client);

            $google_user = $plus->people->get('me');
//print_r($google_user);exit;
            $id = $google_user['id'];

            $email = $google_user['emails'][0]['value'];
            $first_name = $google_user['name']['givenName'];
            $last_name = $google_user['name']['familyName'];
//            $userModel = new User();
//            $userModel->name = $first_name.' '.$last_name;
//            $userModel->google_id = $id;
//            $userModel->email = $email;
//            $userModel->save();
            $calendars=$calendar->calendarList->get('primary');
            print_r($calendars);
            exit;
            $has_user = $user->where('email', '=', $email)->first();

            if (!$has_user) {
                //not yet registered
                $user->email = $email;
                $user->first_name = $first_name;
                $user->last_name = $last_name;
                $user->token = json_encode($token);
                $user->save();
                $user_id = $user->id;

                //create primary calendar
                $calendar = new Calendar;
                $calendar->user_id = $user_id;
                $calendar->title = 'Primary Calendar';
                $calendar->calendar_id = 'primary';
                $calendar->sync_token = '';
                $calendar->save();
            } else {
                $user_id = $has_user->id;
            }

        } else {
            $client = $client->setAccessToken(json_decode(file_get_contents('client_id.json'), true));
            $auth_url = $client->createAuthUrl();
            return redirect($auth_url);
        }



        //        try {
//            $user = Socialite::driver('google')->user();
//
//            $userModel = new User();
//            $userModel->name = $user->name;
//            $userModel->google_id = $user->id;
//            $userModel->email = $user->email;
//            $userModel->save();
//            Auth::loginUsingId($userModel->id);
//            return redirect()->route('home');
//        } catch (InvalidStateException $e) {
//            print_r($e->getMessage());exit;
//            return redirect('/');
//        }
    }
}
