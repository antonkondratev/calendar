@extends('layouts.app')
@section('content')
     <script src="{{asset('/fullcalendar-3.9.0/lib/jquery.min.js')}}"></script>
    <script src="{{asset('/fullcalendar-3.9.0/lib/jquery-ui.min.js')}}"></script>
    <script src="{{asset('/fullcalendar-3.9.0/lib/moment.min.js')}}"></script>
    <script src="{{asset('/fullcalendar-3.9.0/fullcalendar.min.js')}}"></script>
    <script src="{{asset('/fullcalendar-3.9.0/gcal.min.js')}}"></script>
    <link href="{{asset('/fullcalendar-3.9.0/fullcalendar.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                googleCalendarApiKey: '<?= env('GOOGLE_CALENDAR_KEY')?>',
                eventSources: [
                    {
                        googleCalendarId: 'abcd1234@group.calendar.google.com'
                    },
                    {
                        googleCalendarId: 'efgh5678@group.calendar.google.com',
                        className: 'nice-event'
                    }
                ]
            });
            })

        });
    </script>
    <div id="calendar"></div>

@endsection